
MaterialApp(
 title: 'Welcoming Page',
 theme: ThemeData(
 brightness: Brightness.dark,
 primaryColor: Colors.lightBlue[800],
 
 fontFamily: 'Fantasy',
 textTheme: TextTheme(
  headline1: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
 ),
 ),
 home: HomeState(),
);
