import 'package:flutter/material.dart';

void main() => runApp(const SnackBarDemo());

class Design extends StatelessWidget {
  const Design({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Welcoming'),
        ),
        body: const DesignPage(),
      ),
    );
  }
}

class DesignPage extends StatelessWidget {
  const DesignPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        onPressed: () {
          final snackBar = SnackBar(
            content: const Text('Welcome!'),
            action: SnackBarAction(
              label: 'Undo',
              onPressed: () {
              },
            ),
          );

       
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        },
        child: const Text('Login'),
      ),
    );
  }
}
