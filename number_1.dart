import 'package:flutter/material.dart';
import 'package:animated_splash/animated_splash.dart';

void main() {
Function duringSplash = () {
	print('background process');
	int a = 123 + 23;
	print(a);

	if (a > 100)
	return 1;
	else
	return 2;
};

Map<int, Widget> op = {1: Home(), 2: HomeSt()};

runApp(MaterialApp(
	home: AnimatedSplash(
	imagePath: 'cartoon.png',
	home: Home(),
	customFunction: duringSplash,
	duration: 3000,
	type: AnimatedSplashType.BackgroundProcess,
	outputAndHome: op,
	),
));
}

class Home extends StatefulWidget {
@override
_HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
@override
Widget build(BuildContext context) {
	return Scaffold(
		appBar: AppBar(
		title: Text('Welcome!'),
		backgroundColor: Colors.blue,
		),
		body: Center(
			child: Text('Home Page',
				style: TextStyle(color: Colors.black,
					fontSize: 15.0))));
}
}
